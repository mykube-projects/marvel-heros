import 'package:flutter/material.dart';
import 'package:marvelheros/models/MarvelHero.dart';
import 'package:marvelheros/screens/HeroDetail.dart';

class HeroCardWidget extends StatelessWidget {
  final MarvelHero _hero;

  const HeroCardWidget(this._hero);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0.0),
      child: GestureDetector(
        onTap: () =>
            Navigator.push(context, MaterialPageRoute(builder: (context) {
          return HeroDetail(_hero);
        })),
        child: Card(
            elevation: 5.0,
            child: Column(children: [
              Row(
                children: [
                  Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 12.0, 0),
                      child: Column(
                        children: <Widget>[
                          Image.network(_hero.mainImage),
                        ],
                      )),
                  Flexible(
                      child: Column(children: <Widget>[
                    Text(_hero.name),
                    Divider(color: Colors.grey),
                    Text(_hero.description)
                  ])),
                ],
              )
            ])),
      ),
    );
  }
}
