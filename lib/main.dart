import 'package:flutter/material.dart';
import 'package:marvelheros/screens/Home.dart';

Future main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Movie Rating',
      theme: new ThemeData.dark(),
      home: HomeScreen(),
    );
  }
}
