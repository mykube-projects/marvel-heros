import 'package:marvelheros/helpers/NetworkHelper.dart';
import 'package:marvelheros/models/MarvelHero.dart';
import 'package:marvelheros/shared/config.dart';

class MarvelHerosListApi {
  MarvelHerosListApi();

  getHeros(pageNumber, defaultHerosPerPageCount, {String query: ''}) async {
    bool _hasMore = false;
    bool _error = false;
    List _results;
    Secret config = await SecretLoader(secretPath: './assets/.env.json').load();

    final params =
        await _makeParams(pageNumber, defaultHerosPerPageCount, query: query);
    var helper = NetworkHelper(
        url: config.marvelCharactersUrl, path: 'heros/', params: params);

    var resp = await helper.getData();

    if (resp == null) {
      _error = true;
      _results = [];
    } else {
      _hasMore = _hasMoreHeros(resp, pageNumber, defaultHerosPerPageCount);
      _results = resp['results'];
    }
    return [_error, _hasMore, _makeHeros(_results, config)];
  }

  _hasMoreHeros(resp, pageNumber, defaultHerosPerPageCount) {
    return resp['has_more'];
  }

  _makeHeros(List heros, Secret config) {
    List<MarvelHero> _marvelHeros = [];

    heros.forEach((item) => _marvelHeros.add(MarvelHero(
        item['id'],
        item['name'],
        item['description'] != null ? item['description'] : 'Empty Description',
        item['mainImage'],
        item['largeImage'])));

    return _marvelHeros;
  }

  _makeParams(pageNumber, defaultHerosPerPageCount, {query: ''}) async {
    Map<String, dynamic> queryParameters = {
      "per_page": defaultHerosPerPageCount.toString(),
      "page": pageNumber.toString(),
    };

    if (!query.isEmpty) {
      queryParameters['query'] = query;
    }

    return queryParameters;
  }
}
