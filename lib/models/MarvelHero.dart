class MarvelHero {
  int _id;
  String _name;
  String _description;
  String _mainImage;
  String _largeImage;

  MarvelHero(this._id, this._name, this._description, this._mainImage,
      this._largeImage);

  @override
  String toString() => _name;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  String get mainImage => _mainImage;
  String get largeImage => _largeImage;
}
