import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkHelper {
  final String url;
  final String path;
  final Map<String, dynamic> params;
  NetworkHelper({this.url, this.path, this.params});

  Future getData() async {
    final _url = Uri.http(url, path, params);

    http.Response response = await http.get(_url);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      print(response.statusCode);
      return null;
    }
  }
}
