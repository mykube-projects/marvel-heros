import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:marvelheros/models/MarvelHero.dart';
import 'package:marvelheros/services/MarvelHeroListApi.dart';
import 'package:marvelheros/widgets/HeroCard.dart';

class HomeScreen extends StatefulWidget {
  static const String _title = 'Heróis Marvel';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _hasMore;
  int _pageNumber;
  bool _error;
  bool _loading;
  final int _defaultHerosPerPageCount = 20;
  List<MarvelHero> _marvelHeros;
  final int _nextPageThreshold = 5;
  SearchBar searchBar;

  @override
  void initState() {
    super.initState();
    _hasMore = true;
    _pageNumber = 1;
    _error = false;
    _loading = true;
    _marvelHeros = [];
    fetchHeros();
  }

  void searchHeros(query) {
    setState(() {
      _pageNumber = 1;
    });
    fetchSearchHeros(query: query);
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        // title: new Text(HomeScreen._title),
        title: InkWell(
          child: Text(HomeScreen._title),
          onTap: () {
            setState(() {
              _hasMore = true;
              _pageNumber = 1;
              _error = false;
              _loading = true;
              _marvelHeros = [];
            });
            fetchHeros();
          },
        ),
        actions: [searchBar.getSearchAction(context)]);
  }

  _HomeScreenState() {
    searchBar = new SearchBar(
        inBar: true,
        setState: setState,
        onSubmitted: searchHeros,
        buildDefaultAppBar: buildAppBar,
        onClosed: () {
          setState(() {
            _hasMore = true;
            _pageNumber = 1;
            _error = false;
            _loading = true;
            _marvelHeros = [];
          });
          fetchHeros();
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: searchBar.build(context), body: getBody());
  }

  getBody() {
    if (_marvelHeros.isEmpty) {
      if (_loading) {
        return Center(
            child: Padding(
          padding: const EdgeInsets.all(8),
          child: CircularProgressIndicator(),
        ));
      } else if (_error) {
        return Center(
            child: InkWell(
          onTap: () {
            setState(() {
              _loading = true;
              _error = false;
              fetchHeros();
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Text("Error while loading photos, tap to try again"),
          ),
        ));
      } else {
        return Center(
            child: Padding(
          padding: const EdgeInsets.all(8),
          child: Text("No results found"),
        ));
      }
    } else {
      return ListView.builder(
        itemCount: _marvelHeros.length + (_hasMore ? 1 : 0),
        itemBuilder: (BuildContext context, int index) {
          if (index == _marvelHeros.length - _nextPageThreshold && _hasMore) {
            fetchHeros();
          }

          if (index == _marvelHeros.length) {
            if (_error) {
              return Center(
                  child: InkWell(
                onTap: () {
                  setState(() {
                    _loading = true;
                    _error = false;
                    fetchHeros();
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text("Error while loading photos, tap to try agin"),
                ),
              ));
            } else {
              return Center(
                  child: Padding(
                padding: const EdgeInsets.all(8),
                child: CircularProgressIndicator(),
              ));
            }
          }
          return HeroCardWidget(_marvelHeros[index]);
        },
      );
    }
  }

  Future<void> fetchHeros({String query: ''}) async {
    final herosApi = await MarvelHerosListApi()
        .getHeros(_pageNumber, _defaultHerosPerPageCount, query: query);

    setState(() {
      _error = herosApi[0];
      _hasMore = herosApi[1];
      _loading = false;
      _pageNumber = _pageNumber + 1;
      _marvelHeros.addAll(herosApi[2]);
    });
  }

  Future<void> fetchSearchHeros({String query: ''}) async {
    final herosApi = await MarvelHerosListApi()
        .getHeros(_pageNumber, _defaultHerosPerPageCount, query: query);

    setState(() {
      _error = herosApi[0];
      _hasMore = false;
      _loading = false;
      _pageNumber = _pageNumber + 1;
      _marvelHeros = herosApi[2];
    });
  }
}
