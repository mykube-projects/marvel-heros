import 'package:flutter/material.dart';
import 'package:marvelheros/models/MarvelHero.dart';

class HeroDetail extends StatefulWidget {
  final MarvelHero _hero;
  const HeroDetail(this._hero);

  @override
  _HeroDetailState createState() => _HeroDetailState();
}

class _HeroDetailState extends State<HeroDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget._hero.name),
        ),
        body: getBody());
  }

  Widget getBody() {
    return Container(
      padding: EdgeInsets.all(9.0),
      child: Column(
        children: [
          Center(
            child: Image.network(widget._hero.largeImage),
          ),
          Divider(),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(widget._hero.description),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
