import json
import os

f = open('./assets/.env_example.json',)
data = json.load(
   f
)
envData={}
for i in data.keys():
    envData[i] = os.environ[i]
  
f.close()

f = open("./assets/.env.json", "a")
f.write(json.dumps(envData))
f.close()